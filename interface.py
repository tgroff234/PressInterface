#!/usr/bin/python3

import os
import pygame
import time
import random

class pyscope:
    screen = None

    def __init__(self):
        drivers = ['fbcon', 'directfb', 'svgalib']
        os.putenv("SDL_FBDEV", "/dev/fb0")
        found = False
        for driver in drivers:
            os.putenv("SDL_VIDEODRIVER", driver)
            try:
                pygame.display.init()
            except pygame.error:
                print("driver %s failed"%(driver,))
                continue
            found = True
            break

        if not found:
            raise Exception("No suitable display driver")

    def __del__(self):
        pass

    def test(self):
        self.screen.fill((255,0,0))
        pygame.display.update()

scope = pyscope()
scope.test()
time.sleep(10)

